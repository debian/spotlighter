/*
 * Copyright (C) Pietro Pilolli 2010 <pilolli.pietro@gmail.com>
 * 
 * Spotlighter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Spotlighter is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 

#include "utils.h"


/* Set the cairo surface color to the RGBA string. */
void
cairo_set_source_color_from_string     (cairo_t  *cr,
                                        char     *color)
{
  if (cr)
    {
      gint r,g,b,a;
      sscanf (color, "%02X%02X%02X%02X", &r, &g, &b, &a);
      cairo_set_source_rgba (cr,
                             ((gdouble) r) / 256,
                             ((gdouble) g) / 256,
                             ((gdouble) b) / 256,
                             ((gdouble) a) / 256);
    }
}


/* Print cross button to quit the program. */
void
print_close_button           (cairo_t  *cr,
                              gint      width)
{
  gchar *white = "FFFFFFFF";
  gint delta = 25;

  cairo_set_source_color_from_string (cr, white);
	
  cairo_rectangle (cr, width-delta, 0, delta, delta);	
  cairo_move_to (cr, width-delta, 0);
  cairo_line_to (cr, width, delta);

  cairo_move_to (cr, width, 0);
  cairo_line_to (cr, width-delta, delta);
	
  cairo_stroke (cr);
}


/* Color the window with the rgba colour passed. */
void
color_window (cairo_t  *cr,
              gchar    *rgba,
              gint      width)
{
  if (cr)
    {
      cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
      cairo_set_source_color_from_string (cr, rgba);
      cairo_paint (cr);
      cairo_stroke (cr);
      print_close_button (cr, width);
    }
}


/* This draw an ellipse taking the top left edge coordinates the width and the eight of the bounded rectangle. */
void
cairo_draw_rectangle    (cairo_t  *cr,
                         gdouble   x,
                         gdouble   y,
                         gdouble   width,
                         gdouble   height,
                         gchar    *rgba)
{
  if (cr)
    {
      cairo_save (cr);
      cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
      cairo_set_source_color_from_string (cr, rgba);
      cairo_rectangle (cr, x, y, width, height);
      cairo_fill (cr);
      cairo_stroke (cr);
      cairo_restore (cr);
    }
}


/* This draw a rectangle. */
void
cairo_draw_ellipse      (cairo_t  *cr,
                         gdouble   x,
                         gdouble   y,
                         gdouble   width,
                         gdouble   height,
                         gchar *rgba)
{
  if (cr)
    {
      cairo_save (cr);
      cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
      cairo_set_source_color_from_string (cr, rgba);
      cairo_translate (cr, x + width / 2., y + height / 2.);
      cairo_scale (cr, width / 2., height / 2.);
      cairo_arc (cr, 0., 0., 1., 0., 2 * M_PI);
      cairo_fill (cr);
      cairo_stroke (cr);
      cairo_restore (cr);
    }
}

