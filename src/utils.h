/*
 * Copyright (C) Pietro Pilolli 2010 <pilolli.pietro@gmail.com>
 * 
 * Spotlighter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Spotlighter is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <gtk/gtk.h>
#include <math.h>

#include <cairo.h>


/* Set the cairo surface color to the RGBA string. */
void
cairo_set_source_color_from_string     (cairo_t  *cr,
                                        char     *color);


/* Color the window with the rgba colour passed. */
void color_window       (cairo_t  *cr,
                         gchar    *rgba,
                         gint      width);


/*
 * This draw an ellipse taking the top left edge coordinates,
 * the width and the eight of the bounded rectangle.
 */
void cairo_draw_ellipse      (cairo_t  *cr,
                              gdouble   x,
                              gdouble   y,
                              gdouble   width,
                              gdouble   height,
                              gchar    *rgba);


/* This draw a rectangle. */
void cairo_draw_rectangle    (cairo_t  *cr,
                              gdouble   x,
                              gdouble   y,
                              gdouble   width,
                              gdouble   height,
                              gchar    *rgba);


